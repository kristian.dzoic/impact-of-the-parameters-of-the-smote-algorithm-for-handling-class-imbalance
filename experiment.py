import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from imblearn.under_sampling import RandomUnderSampler
from smote import smote
from imblearn.metrics import geometric_mean_score
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB,BernoulliNB
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier

datasets = {
    'Pima': 'data/pima.csv',
    'Vehicle1': 'data/vehicle1.csv',
    'Yeast3': 'data/yeast3.csv',
    'Shuttle': 'data/shuttle-c0-vs-c4.csv',
    'Wine_Red': 'data/winequality-red-4.csv',
    'Poker': 'data/poker-8_vs_6.csv',
    'Abalone19': 'data/abalone19.csv',
}

classifiers = {
    'Naive Bayes (Bernoulli)': BernoulliNB(),
    'Decision Tree': DecisionTreeClassifier(),
    '5-NN': KNeighborsClassifier(n_neighbors=5),
    'SVM': SVC(),
}

def evaluate_model(model, X_train, y_train, X_test, y_test):
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    
    f1 = f1_score(y_test, y_pred, average='macro')
    g_mean = geometric_mean_score(y_test, y_pred, average='macro')
    
    return f1, g_mean

# Separate dictionaries for F1 scores and G-Mean scores
f1_results = {name: pd.DataFrame() for name in classifiers}
gmean_results = {name: pd.DataFrame() for name in classifiers}

n_iterations = 30
parameter_settings = ['Baseline'] + ['Undersampling'] + [f'SMOTE(k={k}, samples={samples})' for k in [3, 5, 7, 10] for samples in [1, 3, 5, 10, 15, 20, 30]]

for dataset_name, dataset_path in datasets.items():
    print(f"Running on {dataset_name}")
    
    data = pd.read_csv(dataset_path)
    label_mapping = {' negative': 0, ' positive': 1}
    data['Class'] = data['Class'].map(label_mapping)

    # Initialize results dictionary for each classifier and parameter setting
    results = {name: {param: {'f1': [], 'g_mean': []} for param in parameter_settings} for name in classifiers}

    # Run multiple iterations
    for i in range(n_iterations):
        print(f"Iteration {i+1}/{n_iterations}")

        train_df, test_df = train_test_split(data, test_size=0.25, stratify=data['Class'], random_state=i)
        X_train = train_df.drop(columns=['Class'])
        y_train = train_df['Class']
        X_test = test_df.drop(columns=['Class'])
        y_test = test_df['Class']

        scaler = StandardScaler()
        X_train_scaled = scaler.fit_transform(X_train)
        X_test_scaled = scaler.transform(X_test)

        # Baseline
        for name, clf in classifiers.items():
            f1, g_mean = evaluate_model(clf, X_train_scaled, y_train, X_test_scaled, y_test)
            results[name]['Baseline']['f1'].append(f1)
            results[name]['Baseline']['g_mean'].append(g_mean)

        # Undersampling
        rus = RandomUnderSampler(random_state=i)
        X_rus, y_rus = rus.fit_resample(X_train_scaled, y_train)
        for name, clf in classifiers.items():
            f1, g_mean = evaluate_model(clf, X_rus, y_rus, X_test_scaled, y_test)
            results[name]['Undersampling']['f1'].append(f1)
            results[name]['Undersampling']['g_mean'].append(g_mean)

        # SMOTE
        for k in [3, 5, 7, 10]:
            for samples in [1, 3, 5, 10, 15, 20, 30]: 
                smote_data = smote(train_df, k, samples)
                X_smote = smote_data.drop('Class', axis=1)
                y_smote = smote_data['Class']

                X_smote_scaled = scaler.fit_transform(X_smote)

                param_name = f'SMOTE(k={k}, samples={samples})'
                for name, clf in classifiers.items():
                    f1, g_mean = evaluate_model(clf, X_smote_scaled, y_smote, X_test_scaled, y_test)
                    results[name][param_name]['f1'].append(f1)
                    results[name][param_name]['g_mean'].append(g_mean)

    # Compile results for each classifier and parameter setting
    for name in classifiers:
        f1_classifier_results = []
        gmean_classifier_results = []
        for param in parameter_settings:
            f1_mean = np.mean(results[name][param]['f1'])
            gmean_mean = np.mean(results[name][param]['g_mean'])
            f1_std = np.std(results[name][param]['f1'])
            gmean_std = np.std(results[name][param]['g_mean'])

            f1_classifier_results.append({
                f'{dataset_name} F1 Score': f'{f1_mean:.3f} ± {f1_std:.2f}',
            })
            gmean_classifier_results.append({
                f'{dataset_name} G-Mean': f'{gmean_mean:.3f} ± {gmean_std:.2f}',
            })
        
        # Convert list of dictionaries to DataFrame
        f1_dataset_results = pd.DataFrame(f1_classifier_results, index=parameter_settings)
        gmean_dataset_results = pd.DataFrame(gmean_classifier_results, index=parameter_settings)
        
        # Concatenate with overall results
        f1_results[name] = pd.concat([f1_results[name], f1_dataset_results], axis=1)
        gmean_results[name] = pd.concat([gmean_results[name], gmean_dataset_results], axis=1)

# Save the results separately for F1 scores and G-Mean scores
for name in classifiers:
    f1_results[name].to_csv(f'{name}_f1_results.csv')
    gmean_results[name].to_csv(f'{name}_gmean_results.csv')
