import tkinter as tk
from tkinter import filedialog
from testing import run_experiment_function

def create_gui():
    root = tk.Tk()
    root.title("Konfiguracija eksperimenta")

    # Dataset path
    tk.Label(root, text="Odabir datoteke:").grid(row=0, column=0, padx=10, pady=10, sticky='e')
    dataset_path_entry = tk.Entry(root, width=50)
    dataset_path_entry.grid(row=0, column=1, padx=10, pady=10)

    tk.Button(root, text="Traži", command=lambda: browse_file(dataset_path_entry)).grid(row=0, column=2, padx=10, pady=10)

    # Number of iterations
    tk.Label(root, text="Broj ponavljanja:").grid(row=1, column=0, padx=10, pady=10, sticky='e')
    iterations_entry = tk.Entry(root, width=10)
    iterations_entry.grid(row=1, column=1, padx=10, pady=10)
    iterations_entry.insert(0, "30")  # Default value

    # Train-test split percentage
    tk.Label(root, text="Veličina podskupa za treniranje:").grid(row=2, column=0, padx=10, pady=10, sticky='e')
    train_test_split_entry = tk.Entry(root, width=10)
    train_test_split_entry.grid(row=2, column=1, padx=10, pady=10)
    train_test_split_entry.insert(0, "75")  # Default value

    # Standardize data
    tk.Label(root, text="Standardizacija:").grid(row=3, column=0, padx=10, pady=10, sticky='e')
    standardize_var = tk.BooleanVar(value=True)
    standardize_check = tk.Checkbutton(root, variable=standardize_var)
    standardize_check.grid(row=3, column=1, padx=10, pady=10)

    # Baseline classification option
    tk.Label(root, text="Baseline:").grid(row=4, column=0, padx=10, pady=10, sticky='e')
    baseline_var = tk.BooleanVar(value=True)
    baseline_check = tk.Checkbutton(root, variable=baseline_var)
    baseline_check.grid(row=4, column=1, padx=10, pady=10)

    # Undersampling option
    tk.Label(root, text="Poduzorkovanje:").grid(row=5, column=0, padx=10, pady=10, sticky='e')
    undersampling_var = tk.BooleanVar(value=False)
    undersampling_check = tk.Checkbutton(root, variable=undersampling_var)
    undersampling_check.grid(row=5, column=1, padx=10, pady=10)

    # SMOTE parameters
    tk.Label(root, text="SMOTE k:").grid(row=6, column=0, padx=10, pady=10, sticky='e')
    smote_k_entry = tk.Entry(root, width=10)
    smote_k_entry.grid(row=6, column=1, padx=10, pady=10)
    smote_k_entry.insert(0, "5")  # Default value

    tk.Label(root, text="SMOTE q:").grid(row=7, column=0, padx=10, pady=10, sticky='e')
    smote_q_entry = tk.Entry(root, width=10)
    smote_q_entry.grid(row=7, column=1, padx=10, pady=10)
    smote_q_entry.insert(0, "10")  # Default value

    # Classifier selection
    tk.Label(root, text="Odabir klasifikatora:").grid(row=8, column=0, padx=10, pady=10, sticky='e')
    classifier_var = tk.StringVar(value="Naive Bayes (Bernoulli)")  # Default value

    def on_classifier_change(*args):
        if classifier_var.get() == "k-NN":
            neighbors_entry.grid(row=9, column=1, padx=10, pady=10)  # Show the neighbors input
            neighbors_label.grid(row=9, column=0, padx=10, pady=10, sticky='e')
        else:
            neighbors_entry.grid_remove()  # Hide the neighbors input
            neighbors_label.grid_remove()

    classifier_var.trace('w', on_classifier_change)
    classifier_menu = tk.OptionMenu(root, classifier_var, "Naive Bayes (Bernoulli)", "Decision Tree", "k-NN", "SVM")
    classifier_menu.grid(row=8, column=1, padx=10, pady=10)

    # k-NN neighbors input (initially hidden)
    neighbors_label = tk.Label(root, text="k-NN broj susjeda:")
    neighbors_entry = tk.Entry(root, width=10)
    neighbors_entry.insert(0, "5")  # Default value
    neighbors_entry.grid_remove()
    neighbors_label.grid_remove()

    # Run button
    def run_experiment():
        dataset_path = dataset_path_entry.get()
        iterations = int(iterations_entry.get())
        smote_k = int(smote_k_entry.get())
        smote_q = int(smote_q_entry.get())
        selected_classifier = classifier_var.get()
        neighbors = int(neighbors_entry.get()) if classifier_var.get() == "k-NN" else None
        use_baseline = baseline_var.get()
        use_undersampling = undersampling_var.get()
        train_test_split_percentage = float(train_test_split_entry.get()) / 100
        standardize_data = standardize_var.get()

        # Call the experiment function
        result_text = run_experiment_function(
            dataset_path=dataset_path,
            iterations=iterations,
            smote_k=smote_k,
            smote_q=smote_q,
            selected_classifiers=[selected_classifier],
            neighbors=neighbors,
            use_baseline=use_baseline,
            use_undersampling=use_undersampling,
            train_test_split_percentage=train_test_split_percentage,
            standardize_data=standardize_data
        )

        # Display results
        result_text_display.config(state='normal')
        result_text_display.delete('1.0', tk.END)
        result_text_display.insert(tk.END, result_text)
        result_text_display.config(state='disabled')

    run_button = tk.Button(root, text="Pokreni eksperiment", command=run_experiment)
    run_button.grid(row=10, column=0, columnspan=2, padx=10, pady=10)

    # Results display
    result_text_display = tk.Text(root, height=20, width=80, state='disabled')
    result_text_display.grid(row=11, column=0, columnspan=3, padx=10, pady=10)

    root.mainloop()

def browse_file(entry_widget):
    filename = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
    if filename:
        entry_widget.delete(0, tk.END)
        entry_widget.insert(0, filename)

if __name__ == "__main__":
    create_gui()