import matplotlib.colors
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from smote import smote

data = pd.read_csv('data/yeast3.csv')
label_mapping = {' negative': 0, ' positive': 1}
data['Class'] = data['Class'].map(label_mapping)

plt.figure(1)
plt.scatter(data.iloc[:,2], data.iloc[:, 1], c=data['Class'], cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.xlim(0,1)
plt.ylim(0,1)
smote_data = smote(data,5,22)

plt.figure(2)
plt.scatter(smote_data.iloc[:,2], smote_data.iloc[:, 1], c=smote_data['Class'], cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.xlim(0,1)
plt.ylim(0,1)
plt.show()