import matplotlib.colors
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from smote import smote
from sklearn.model_selection import train_test_split
from imblearn.under_sampling import RandomUnderSampler

data = pd.read_csv('data/yeast1.csv')

label_mapping = {' negative': 0, ' positive': 1}
data['Class'] = data['Class'].map(label_mapping)

train_df, test_df = train_test_split(data, test_size=0.25, stratify=data['Class'], random_state=42)

X_train = train_df.drop(columns=['Class'])
y_train = train_df['Class']

X_test = test_df.drop(columns=['Class'])
y_test = test_df['Class']

plt.figure(1)
plt.scatter(X_train.iloc[:,2], X_train.iloc[:, 1], c=y_train, cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.xlim(0,1)
plt.ylim(0,1)

undersampler = RandomUnderSampler(random_state=42)
X_train_resampled, y_train_resampled = undersampler.fit_resample(X_train, y_train)

print("Class distribution before undersampling:\n", y_train.value_counts())
print("Class distribution after undersampling:\n", y_train_resampled.value_counts())

plt.figure(2)
plt.scatter(X_train_resampled.iloc[:,2], X_train_resampled.iloc[:, 1], c=y_train_resampled, cmap=matplotlib.colors.ListedColormap(['red', 'blue']))
plt.xlim(0,1)
plt.ylim(0,1)       
plt.show()
