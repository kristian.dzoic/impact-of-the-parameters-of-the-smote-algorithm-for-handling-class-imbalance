import pandas as pd
import numpy as np
from smote import smote
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, confusion_matrix
from imblearn.under_sampling import RandomUnderSampler
from imblearn.metrics import geometric_mean_score
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import BernoulliNB
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier

def run_experiment_function(dataset_path, iterations, smote_k, smote_q, selected_classifiers, neighbors, use_baseline, use_undersampling, train_test_split_percentage, standardize_data):
    data = pd.read_csv(dataset_path)
    label_mapping = {' negative': 0, ' positive': 1}
    data['Class'] = data['Class'].map(label_mapping)

    classifiers = {
        'Naive Bayes (Bernoulli)': BernoulliNB(),
        'Decision Tree': DecisionTreeClassifier(),
        'k-NN': KNeighborsClassifier(n_neighbors=neighbors) if neighbors else None,
        'SVM': SVC(),
    }
    
    selected_classifiers_dict = {name: classifiers[name] for name in selected_classifiers if classifiers[name]}

    def evaluate_model(model, X_train, y_train, X_test, y_test):
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        f1 = f1_score(y_test, y_pred, average='macro')
        g_mean = geometric_mean_score(y_test, y_pred, average='macro')
        conf_matrix = confusion_matrix(y_test, y_pred)
        return f1, g_mean, conf_matrix

    # Initialize dictionaries to store results
    results = {
        'Baseline': {name: {'f1': [], 'gmean': [], 'conf_matrix': []} for name in selected_classifiers},
        'Undersampling': {name: {'f1': [], 'gmean': [], 'conf_matrix': []} for name in selected_classifiers},
        'SMOTE': {name: {'f1': [], 'gmean': [], 'conf_matrix': []} for name in selected_classifiers}
    }

    for i in range(iterations):
        # Train-test split
        train_df, test_df = train_test_split(data, test_size=1-train_test_split_percentage, stratify=data['Class'], random_state=i)
        X_train = train_df.drop(columns=['Class'])
        y_train = train_df['Class']
        X_test = test_df.drop(columns=['Class'])
        y_test = test_df['Class']

        # Standardize data if selected
        if standardize_data:
            scaler = StandardScaler()
            X_train = scaler.fit_transform(X_train)
            X_test = scaler.transform(X_test)
        else:
            scaler = None

        # Evaluate Baseline
        if use_baseline:
            for name, clf in selected_classifiers_dict.items():
                f1, g_mean, conf_matrix = evaluate_model(clf, X_train, y_train, X_test, y_test)
                results['Baseline'][name]['f1'].append(f1)
                results['Baseline'][name]['gmean'].append(g_mean)
                results['Baseline'][name]['conf_matrix'].append(conf_matrix)

        # Evaluate Undersampling
        if use_undersampling:
            rus = RandomUnderSampler(random_state=i)
            X_rus, y_rus = rus.fit_resample(X_train, y_train)
            for name, clf in selected_classifiers_dict.items():
                f1, g_mean, conf_matrix = evaluate_model(clf, X_rus, y_rus, X_test, y_test)
                results['Undersampling'][name]['f1'].append(f1)
                results['Undersampling'][name]['gmean'].append(g_mean)
                results['Undersampling'][name]['conf_matrix'].append(conf_matrix)

        # Evaluate SMOTE
        if smote_k and smote_q:
            for name, clf in selected_classifiers_dict.items():
                smote_data = smote(train_df, smote_k, smote_q)
                X_smote = smote_data.drop('Class', axis=1)
                y_smote = smote_data['Class']
                if scaler:
                    X_smote = scaler.fit_transform(X_smote)
                f1, g_mean, conf_matrix = evaluate_model(clf, X_smote, y_smote, X_test, y_test)
                results['SMOTE'][name]['f1'].append(f1)
                results['SMOTE'][name]['gmean'].append(g_mean)
                results['SMOTE'][name]['conf_matrix'].append(conf_matrix)

    # Calculate mean results for each method and classifier
    f1_means = {method: {name: np.mean(scores['f1']) for name, scores in results[method].items()} for method in results}
    gmean_means = {method: {name: np.mean(scores['gmean']) for name, scores in results[method].items()} for method in results}

    # Format results for display
    formatted_results = ""
    for method in results:
        formatted_results += f"\n{method} Rezultati:\n"
        for clf in selected_classifiers:
            if clf in results[method]:
                f1_mean = f1_means[method].get(clf, 'N/A')
                gmean_mean = gmean_means[method].get(clf, 'N/A')
                formatted_results += f"{clf} - F1 Mjera: {f1_mean:.3f}, G-Mean Mjera: {gmean_mean:.3f}\n"
 

    return formatted_results
