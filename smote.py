import pandas as pd
import numpy as np
from sklearn.neighbors import NearestNeighbors

def smote(df, k=5, q=1):
    target_column = df.columns[-1]
    minority_class = df[target_column].value_counts().idxmin()
    majority_class = df[target_column].value_counts().idxmax()
    df_minority = df[df[target_column] == minority_class]
    df_majority = df[df[target_column] == majority_class]
    X_minority = df_minority.drop(columns=[target_column])
    y_minority = df_minority[target_column]

    nn = NearestNeighbors(n_neighbors=k+1)
    nn.fit(X_minority)
    nns = nn.kneighbors(X_minority, return_distance=False)
    synthetic_samples = []
    
    for i in range(len(X_minority)):
        for j in range(q):
            neighbor_idx = np.random.choice(nns[i][1:]) #iskljuci prvog jer je to sam uzorak
            neighbor = X_minority.iloc[neighbor_idx]
            rand = np.random.rand()
            synthetic_sample = {}
            for feature in X_minority.columns:
                diff = neighbor[feature] - X_minority.iloc[i][feature]
                synthetic_value = X_minority.iloc[i][feature] + rand * diff
                synthetic_sample[feature] = synthetic_value
            synthetic_samples.append(synthetic_sample)
    
    synthetic_samples_df = pd.DataFrame(synthetic_samples, columns=X_minority.columns)
    synthetic_samples_df[target_column] = minority_class
    df_smote = pd.concat([df_majority, df_minority, synthetic_samples_df], ignore_index=True)
    return df_smote